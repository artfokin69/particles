import * as THREE from 'three';
import gsap from 'gsap';
import * as dat from 'dat.gui';
const autoBind = require('auto-bind');
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import fragment from './shaders/fragment.frag';
import vertex from './shaders/vertex.vert';
import img1 from './assets/img1.png';
import img2 from './assets/img2.png'
import maskTexture from './assets/mask.jpg';

const textureLoader = new THREE.TextureLoader();
export default class Sketch{
  camera =  new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 0.1, 3000 );
  scene = new THREE.Scene();
  renderer = new THREE.WebGLRenderer( { antialias: true } );
  time = 0;
  move = 0;
  mesh: THREE.Points<THREE.BufferGeometry, THREE.ShaderMaterial>;
  orbitControls: OrbitControls;
  textures: THREE.Texture[];
  mask: THREE.Texture;
  material: THREE.ShaderMaterial;
  raycaster = new THREE.Raycaster();
  mouse = new THREE.Vector2();
  point = new THREE.Vector2();
  settings = {
    progress:0,
  };

  constructor(){
    autoBind(this);
    this.initThree();
    // this.orbitControls = new OrbitControls( this.camera, this.renderer.domElement );

    Promise.all([
      loadTexture(img1), 
      loadTexture(img2),
      loadTexture(maskTexture)
    ]).then(([t1,t2,mask]) => {
      this.textures = [t1,t2];
      this.mask = mask;
      this.addMesh()
    })
    
    
    this.time = 0;
    this.render();

    window.addEventListener('resize', this.resizeHandler);
    this.resizeHandler();
    this.mouseEffects();
    this.initGui();
  }

  initThree(){
    this.camera.position.z = 1000;
    this.scene.background = new THREE.Color( 0xbfe3dd );
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize( window.innerWidth, window.innerHeight );
    this.renderer.outputEncoding = THREE.sRGBEncoding;
    document.body.appendChild( this.renderer.domElement );
  }

  addMesh(){
    const geometry = new THREE.BufferGeometry( );
    const numberPerSide = 512;
    const number = numberPerSide * numberPerSide;
    const itemSize = 3;

    const positions = new THREE.BufferAttribute(new Float32Array(number * itemSize), itemSize);
    const coordinates = new THREE.BufferAttribute(new Float32Array(number * itemSize), itemSize);
    const speeds = new THREE.BufferAttribute(new Float32Array(number), 1);
    const offsets = new THREE.BufferAttribute(new Float32Array(number), 1);
    const directions = new THREE.BufferAttribute(new Float32Array(number), 1);
    const press = new THREE.BufferAttribute(new Float32Array(number), 1);


    let index = 0;
    for (let i = 0; i < numberPerSide; i++) {
      const centerXShift = i - (numberPerSide / 2);
      for (let j = 0; j < numberPerSide; j++) {
        positions.setXYZ(index, centerXShift, (j - (numberPerSide / 2)), 0);
        coordinates.setXYZ(index, i, j, 0);
        offsets.setX(index, 0);
        speeds.setX(index, rand(0.4, 1));
        directions.setX(index, Math.random() > 0.5 ? 1 : -1)
        press.setX(index, rand(0.4, 1))
        index++;
      } 
    }
    
    geometry.setAttribute("position", positions);
    geometry.setAttribute("aCoordinates", coordinates);
    geometry.setAttribute("aSpeed", speeds);
    geometry.setAttribute("aOffset", offsets);
    geometry.setAttribute("aDirection", directions);
    geometry.setAttribute("aPress" , press);

    this.material = new THREE.ShaderMaterial({
      fragmentShader: fragment,
      vertexShader: vertex,
      uniforms: {
        t1 : {value: this.textures[0]},
        t2 : {value: this.textures[1]},
        mask : {value: this.mask},
        sideSize: {value: numberPerSide},
        time: {value: 0.0},
        move: {value: 0.0},
        cameraZ: {value: this.camera.position.z},
        mouse: {value: {x:0,y:0}},
        mousePressed: {value: 0},
        transition: {value:0},
      },
      side: THREE.DoubleSide,
      transparent: true,
      depthTest: false,
      depthWrite: false,
    })

    this.mesh = new THREE.Points(geometry, this.material);
    this.scene.add(this.mesh);
  }

  resizeHandler(){
    this.updateCamera();
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  }

  updateCamera(){
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
  }

  mouseEffects(){
    const test = new THREE.Mesh(
      new THREE.PlaneBufferGeometry(1000,1000),
      new THREE.MeshBasicMaterial()
    )

    window.addEventListener('mousedown', (e)=>{
      if(this.mesh){
        gsap.to(this.mesh.material.uniforms.mousePressed, {value:1, duration:1,ease: 'elastic.out(1, 0.3)' })
      }
    })

    window.addEventListener('mouseup', (e)=>{
      if(this.mesh){
        gsap.to(this.mesh.material.uniforms.mousePressed, {value:0, duration:1, ease: 'elastic.out(1, 0.3)'})
      }
    })

    window.addEventListener('mousewheel', (e:MouseWheelEvent)=>{
      this.move += e.deltaY / 1000; 
      this.move = Math.max(0, this.move); 
    })
    window.addEventListener( 'mousemove', (e)=>{
      this.mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
      this.mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
      this.raycaster.setFromCamera( this.mouse, this.camera );
      const intersects = this.raycaster.intersectObjects( [test] );
      this.point.x = intersects[0].point.x;
      this.point.y = intersects[0].point.y;
    }, false );
  }
  initGui(){
    const gui = new dat.GUI();
    console.log(this.settings)
    gui.add(this.settings, 'progress', 0, 1, 0.01).name('Zoom vs Loupe');
  }

  render(){ 
    window.requestAnimationFrame(this.render);
    this.time++;
    
    if(this.mesh){
      this.mesh.material.uniforms.time.value = this.time;
      this.mesh.material.uniforms.move.value = this.move;
      this.mesh.material.uniforms.mouse.value = this.point;

      // const next = Math.floor(this.move + 400 ) % 2;
      // const prev = (Math.floor(this.move) + 1 + 400) % 2;
      // this.mesh.material.uniforms.t1.value = this.textures[prev]
      // this.mesh.material.uniforms.t2.value = this.textures[next]
      this.mesh.material.uniforms.transition.value = this.settings.progress;
    }

    if(this.orbitControls){
      this.orbitControls.update();
    }
    

    this.renderer.render( this.scene, this.camera );
  }
}

new Sketch();


function loadTexture(path): Promise<THREE.Texture>{
  return new Promise((resolve,reject)=>{
    textureLoader.load(path, function ( texture ) {
      resolve(texture);
    }, function ( xhr ) {
      console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
    }, function ( error ) {
      console.error( error );
      reject();
    } );
  })
}
const rand = (a, b)=> a + (b - a) * Math.random();