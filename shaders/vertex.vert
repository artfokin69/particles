varying vec2 vUv;
varying vec3 vPos;
varying vec2 vCoordinates;
attribute vec3 aCoordinates;
attribute float aSpeed;
attribute float aOffset;
attribute float aDirection;
attribute float aPress;

uniform float move;
uniform float time;
uniform float cameraZ;
uniform vec2 mouse;
uniform float mousePressed;
uniform float transition;

void main() {
  vUv = uv;
  // SCROLL
  vec3 pos = position;
  pos.x += sin(move * aSpeed) * 3.;
  pos.y += cos(move * aSpeed) * 3.;
  // pos.z = mod(move, 2400.) * aSpeed + position.z;
  pos.z = mod(position.z + move * 1000. * aSpeed, cameraZ);
  
  //LOUPE
  vec3 stable = position;
  float dist = distance(stable.xy, mouse);
  float area = 1. - smoothstep(0., 100., dist);
  stable.x +=  50. * sin(0.1 * time * aPress )* aDirection * area * mousePressed;
  stable.y +=  50. * sin(0.1 * time * aPress )* aDirection * area * mousePressed;
  stable.z +=  100. * sin(0.1 *  time * aPress )* aDirection * area * mousePressed;
  
  vec3 final = mix(pos, stable, transition);
  
  vec4 mvPosition = modelViewMatrix * vec4( final, 1. );
  gl_PointSize = 5000. * ( 1. / - mvPosition.z );
  gl_Position = projectionMatrix * mvPosition;
  
  vCoordinates = aCoordinates.xy;
  vPos = final;
}